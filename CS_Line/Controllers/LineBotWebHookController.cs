﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using isRock.LineBot;
using isRock.LineBot.Conversation;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace CS_Line.Controllers
{

    public class LineBotWebHookController : ApiController
    {
        //string path = "D:\\home\\";// System.Web.HttpContext.Current.Request.MapPath("/temp/");

       // string fileURL;
        // int mode = -1;

        private void SetRegisterStatus(string UserID, bool isRegister)
        {
            System.Web.HttpContext.Current.Application["RegStatus" + UserID] = isRegister;
        }

        private bool getRegisterStatus(string UserID)
        {
            if (System.Web.HttpContext.Current.Application["RegStatus" + UserID] == null)
                System.Web.HttpContext.Current.Application["RegStatus" + UserID] = false;
            return System.Convert.ToBoolean(System.Web.HttpContext.Current.Application["RegStatus" + UserID]);
        }

        private void SetValidCode(string UserID, string validCode)
        {
            System.Web.HttpContext.Current.Application["valid"+UserID] = validCode;
        }

        private string GetValidCode(string UserID)
        {
            if (System.Web.HttpContext.Current.Application["valid" + UserID] == null)
                System.Web.HttpContext.Current.Application["valid" + UserID] = -1;
            return System.Web.HttpContext.Current.Application["valid" + UserID].ToString();
        }

        private void SetUserRegStep(string UserID, int step)
        {
            System.Web.HttpContext.Current.Application["RegStep" + UserID] = step;
        }

        private int GetUserRegStep(string UserID)
        {
            if (System.Web.HttpContext.Current.Application["RegStep" + UserID] == null)
                System.Web.HttpContext.Current.Application["RegStep" + UserID] = -1;
            return Int32.Parse(System.Web.HttpContext.Current.Application["RegStep" + UserID].ToString());
        }

        private void SetUserLeaveStep(string UserID, int step)
        {
            System.Web.HttpContext.Current.Application["LeaveStep" + UserID] = step;
        }

        private int GetUserLeaveStep(string UserID)
        {
            if (System.Web.HttpContext.Current.Application["LeaveStep" + UserID] == null)
                System.Web.HttpContext.Current.Application["LeaveStep" + UserID] = -1;
            return Int32.Parse(System.Web.HttpContext.Current.Application["LeaveStep" + UserID].ToString());
        }

        private void SetUserEmail(string UserID, string email)
        {
            System.Web.HttpContext.Current.Application["email" + UserID] = email;
        }

        private string GetUserEmail(string UserID)
        {
            if (System.Web.HttpContext.Current.Application["email" + UserID] == null)
                System.Web.HttpContext.Current.Application["email" + UserID] = "";
            return System.Web.HttpContext.Current.Application["email" + UserID].ToString();
        }

        private void SetUserName(string UserID, string name)
        {
            System.Web.HttpContext.Current.Application["name" + UserID] = name;
        }

        private string GetUserName(string UserID)
        {
            if (System.Web.HttpContext.Current.Application["name" + UserID] == null)
                System.Web.HttpContext.Current.Application["name" + UserID] = "";
            return System.Web.HttpContext.Current.Application["name" + UserID].ToString();
        }

        private void SetLeaveType(string UserID, string leaveType)
        {
            System.Web.HttpContext.Current.Application["leaveType" + UserID] = leaveType;
        }

        private string GetLeaveType(string UserID)
        {
            if (System.Web.HttpContext.Current.Application["leaveType" + UserID] == null)
                System.Web.HttpContext.Current.Application["leaveType" + UserID] = "";
            return System.Web.HttpContext.Current.Application["leaveType" + UserID].ToString();
        }

        private void SetEndTime(string UserID, DateTime endTime)
        {
            System.Web.HttpContext.Current.Application["endTime" + UserID] = endTime;
        }

        private DateTime GetEndTime(string UserID)
        {
            return Convert.ToDateTime(System.Web.HttpContext.Current.Application["endTime" + UserID]);
        }

        private void SetStartTime(string UserID,DateTime startTime)
        {
            System.Web.HttpContext.Current.Application["startTime" + UserID] = startTime;
        }

        private DateTime GetStartTime(string UserID)
        {
            return Convert.ToDateTime(System.Web.HttpContext.Current.Application["startTime" + UserID]);
        }

        private int checkRegister(string UserID)
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "linedata.database.windows.net";
                builder.UserID = "slow";
                builder.Password = "pP47115670";
                builder.InitialCatalog = "LineBotSql";
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    StringBuilder sb = new StringBuilder();
                    String sql = "SELECT * FROM RegisterTable WHERE lineID='" + UserID + "';";
                    System.Diagnostics.Trace.WriteLine("sql:" + sql);
                    sb.Append(sql);// WHERE lineID=" +UserID
                  
                    System.Diagnostics.Trace.WriteLine("SELECT");
                    using (SqlCommand cmd = new SqlCommand(sb.ToString(), connection))
                    {
                        connection.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                System.Diagnostics.Trace.WriteLine("有資料");
                                while (reader.Read())
                                {
                                    System.Diagnostics.Trace.WriteLine("Read");                                  
                                    string email = reader.GetString(2).ToString();
                                    string name = reader.GetString(3).ToString();
                                    System.Diagnostics.Trace.WriteLine(email + " " + name);
                                    SetUserEmail(UserID, email);
                                    SetUserName(UserID, name);
                                }
                                return 1;
                            }
                            else
                            {
                                System.Diagnostics.Trace.WriteLine("無資料");
                                return 0;
                            }

                        }
                    }


                }
            }
            catch (SqlException e)
            {
                System.Diagnostics.Trace.WriteLine(e.ToString());
                return -1;
                //Console.WriteLine(e.ToString());
            }
        }


        private void insertPendingLeave(string UserID, string leaveType, DateTime startTime, DateTime endTime,int leaveHour, string reason,string certified)
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "linedata.database.windows.net";
                builder.UserID = "slow";
                builder.Password = "pP47115670";
                builder.InitialCatalog = "LineBotSql";
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO PendingLeaveTable(leaveTime, email,name,leaveType,startTime,endTime,leaveHour,reason,certified)");
                    sb.Append("VALUES (@leaveTime,@email,@name,@leaveType,@startTime,@endTime,@leaveHour,@reason,@certified)");
                    using (SqlCommand cmd = new SqlCommand(sb.ToString(), connection))
                    {
                        cmd.Parameters.AddWithValue("@leaveTime", DateTime.Now);
                        cmd.Parameters.AddWithValue("@email", GetUserEmail(UserID));
                        cmd.Parameters.AddWithValue("@name", GetUserName(UserID));
                        cmd.Parameters.AddWithValue("@leaveType", leaveType);
                        cmd.Parameters.AddWithValue("@startTime", startTime);
                        cmd.Parameters.AddWithValue("@endTime", endTime);
                        cmd.Parameters.AddWithValue("@leaveHour", leaveHour);
                        cmd.Parameters.AddWithValue("@reason", reason);
                        cmd.Parameters.AddWithValue("@certified", certified);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        System.Diagnostics.Trace.WriteLine("Insert");
                        connection.Close();
                        SetRegisterStatus(UserID, true);
                    }
                }
            }
            catch (SqlException e)
            {
                System.Diagnostics.Trace.WriteLine(e.ToString());
                //Console.WriteLine(e.ToString());
            }
        }

        [HttpPost]

        public async Task<IHttpActionResult> POSTAsync()
        {
            string ChannelAccessToken = "a1ixz3CwBqf534SBerO0cNdtPtOdwPh9hHT4+7+UHxE6nBS/LRHRlqpvIDciBsMo28p2blyHSvc1CZjV8/ugZw71TetQ/dJY0ClNOM9SwMW8CjQy0v6y5sBP4H3WfsX4YYQa7tKezTOH0+rTjhzvgQdB04t89/1O/w1cDnyilFU=";
            var responseMsg = "";
            var apiKey = Environment.GetEnvironmentVariable("SENDGRID_API_KEY");
            try
            {          
                isRock.LineBot.Bot bot = new isRock.LineBot.Bot(ChannelAccessToken);
                string postData = Request.Content.ReadAsStringAsync().Result;
               // var UserID = isRock.LineBot.Utility.Parsing(postData).events[0].source.userId;
                var ReceivedMessage = isRock.LineBot.Utility.Parsing(postData);
           
               /* var message = ReceivedMessage.events[0].message.text;
                System.Diagnostics.Trace.WriteLine(message);
                */
                var LineEvent = ReceivedMessage.events.FirstOrDefault();
                System.Diagnostics.Trace.WriteLine("type:"+LineEvent.type);
                var UserID = LineEvent.source.userId;
                System.Diagnostics.Trace.WriteLine("UserID:" + UserID);

                if (LineEvent.type == "message")
                {
                    System.Diagnostics.Trace.WriteLine("message type:" + LineEvent.message.type);

                   /* if (LineEvent.message.type=="image")
                    {
                        var filename = Guid.NewGuid().ToString() + ".png";
                        System.Diagnostics.Trace.WriteLine("filename:" + filename);
                        byte[] fileBody = Utility.GetUserUploadedContent(LineEvent.message.id, ChannelAccessToken);
                        System.Diagnostics.Trace.WriteLine("fileBody");
                        //'D:\home\site\wwwroot\temp\672ba3d9-e6c6-4eb1-a5bc-ddd143e3a30c.png'.
                        // D：\ home
                        System.IO.File.WriteAllBytes(path + filename, fileBody);//error
                        System.Diagnostics.Trace.WriteLine("IO");
                        fileURL = $"http://{System.Web.HttpContext.Current.Request.Url.Host}/temp/{filename}";
                        System.Diagnostics.Trace.WriteLine("fileURL:"+ fileURL);
                        Utility.ReplyMessage(LineEvent.replyToken, $"收到一個圖檔，位於:\n{fileURL}", ChannelAccessToken);

                    }*/

                    switch (LineEvent.message.text)
                    {
                        case "Max":
                            System.Diagnostics.Trace.WriteLine("MAX");
                            bot.PushMessage("Ucc7dc59bb947ba6cc817a118e477b6ed", "測試測試");
                            //Ue96b6fc320dc339b3fdca5b864ee0b6b
                            //MAX U72b236422bc5cff5817edcc645c3b419
                            //MAX new  Ucc7dc59bb947ba6cc817a118e477b6ed
                            break;

                        case "我要請假":
                            if(isRegister(UserID)){
                                var ButtonsTpMsg = new ButtonsTemplate();
                                ButtonsTpMsg.altText = "無法顯示時的替代文字";
                                ButtonsTpMsg.text = "請問你要請什麼假別?";
                                ButtonsTpMsg.title = "";
                                var actions = new List<TemplateActionBase>();
                                actions.Add(new PostbackAction() { label = "事假", data = "類型事假" + DateTime.Now });
                                actions.Add(new PostbackAction() { label = "病假", data = "類型病假" + DateTime.Now });
                                actions.Add(new PostbackAction() { label = "公假", data = "類型公假" + DateTime.Now });
                                actions.Add(new PostbackAction() { label = "特休", data = "類型特休" + DateTime.Now });
                                ButtonsTpMsg.actions = actions;
                                SetUserLeaveStep(UserID, 0);
                                bot.PushMessage(UserID, ButtonsTpMsg);
                            }
                                                  
                            break;

                        case "註冊":
                            // isRock.LineBot.Bot bot = new isRock.LineBot.Bot(ChannelAccessToken);
                            if (isRegister(UserID))
                            {

                            }
                            else
                            {
                                bot.PushMessage(UserID, "請問公司的專屬信箱?");
                                SetUserRegStep(UserID, 0);
                            }                      

                            break;

                        case "test":
                            checkRegister(UserID);//0:無資料 1:有資料 -1:例外錯誤
                            
                            break;

                        default:

                           if(isRegister(UserID))
                            {
                                var curStep = GetUserLeaveStep(UserID);
                                switch (curStep)
                                {
                                    case 0:

                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                }
                            }
                            else
                            {
                                var curStep = GetUserRegStep(UserID);

                                switch (curStep)
                                {
                                    case 0:
                                        System.Diagnostics.Trace.WriteLine("信箱:" + LineEvent.message.text);
                                        SetUserRegStep(UserID, 1);
                                        var emailConfirm = new ConfirmTemplate();
                                        emailConfirm.altText = "無法顯示時的替代文字";
                                        emailConfirm.text = "您的信箱為" + LineEvent.message.text + "? ";
                                        var emailConfirmActions = new List<TemplateActionBase>();
                                        emailConfirmActions.Add(new PostbackAction() { label = "確認", data = "信箱確認" + DateTime.Now });
                                        emailConfirmActions.Add(new PostbackAction() { label = "修改", data = "信箱修改" + DateTime.Now });
                                        emailConfirm.actions = emailConfirmActions;
                                        SetUserEmail(UserID, LineEvent.message.text.Trim());
                                        bot.PushMessage(UserID, emailConfirm);
                                        break;
                                    case 1:
                                        var nameConfirm = new ConfirmTemplate();
                                        nameConfirm.altText = "無法顯示時的替代文字";
                                        nameConfirm.text = "您的姓名為" + LineEvent.message.text + "? ";
                                        var nameConfirmtActions = new List<TemplateActionBase>();
                                        nameConfirmtActions.Add(new PostbackAction() { label = "確認", data = "姓名確認" + DateTime.Now });
                                        nameConfirmtActions.Add(new PostbackAction() { label = "修改", data = "姓名修改" + DateTime.Now });
                                        nameConfirm.actions = nameConfirmtActions;
                                        SetUserName(UserID, LineEvent.message.text);
                                        bot.PushMessage(UserID, nameConfirm);
                                        break;
                                    case 2:
                                        System.Diagnostics.Trace.WriteLine("驗證碼為:" + LineEvent.message.text);
                                        if (LineEvent.message.text == GetValidCode(UserID))
                                        {
                                            System.Diagnostics.Trace.WriteLine("驗證成功");
                                            try
                                            {
                                                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                                                builder.DataSource = "linedata.database.windows.net";
                                                builder.UserID = "slow";
                                                builder.Password = "pP47115670";
                                                builder.InitialCatalog = "LineBotSql";
                                                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                                                {
                                                    StringBuilder sb = new StringBuilder();
                                                    sb.Append("INSERT INTO RegisterTable(lineID, email,name,registerTime)");
                                                    sb.Append("VALUES (@lineID,@email,@name,@registerTime)");
                                                    using (SqlCommand cmd = new SqlCommand(sb.ToString(), connection))
                                                    {
                                                        cmd.Parameters.AddWithValue("@lineID", UserID);
                                                        cmd.Parameters.AddWithValue("@email", GetUserEmail(UserID));
                                                        cmd.Parameters.AddWithValue("@name", GetUserName(UserID));
                                                        cmd.Parameters.AddWithValue("@registerTime", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                                                        connection.Open();
                                                        cmd.ExecuteNonQuery();
                                                        System.Diagnostics.Trace.WriteLine("Insert");
                                                        connection.Close();
                                                        SetRegisterStatus(UserID, true);
                                                    }
                                                }
                                            }
                                            catch (SqlException e)
                                            {
                                                System.Diagnostics.Trace.WriteLine(e.ToString());
                                                //Console.WriteLine(e.ToString());
                                            }

                                        }
                                        break;

                                }

                            }


                            break;
                    }

                }else if (LineEvent.type == "postback")
                {
                    System.Diagnostics.Trace.WriteLine("postback Data:" + LineEvent.postback.data);
                    var postD = LineEvent.postback.data;

                  /*  if(postD== "datetimepicker")//開始時間
                    {
                        var time = LineEvent.postback.Params.datetime;
                        System.Diagnostics.Trace.WriteLine("time:" + time);
                    }
                    else
                    {

                    }
                    */
                   
                    var symbol= postD.Substring(0, 4);
                    System.Diagnostics.Trace.WriteLine("symbol:" + symbol);
                    System.Diagnostics.Trace.WriteLine("postD.Length:" + postD.Length);
                    var dataTime = postD.Substring(4, 20);
                   
                    System.Diagnostics.Trace.WriteLine("dataTime:" + dataTime);
                    var now = DateTime.Now;
                    System.Diagnostics.Trace.WriteLine("now:" + now);
                    TimeSpan ts = DateTime.Now - Convert.ToDateTime(dataTime);
                    System.Diagnostics.Trace.WriteLine("ts:" + ts.ToString());
                    var minuteDiff= ts.ToString().Substring(3,2);
                    System.Diagnostics.Trace.WriteLine("minuteDiff:" + minuteDiff);
                    if (Int32.Parse(minuteDiff) > 1)
                    {

                    }
                    else
                    {
                        switch (symbol)
                        {
                            case "信箱確認":
                                SetUserRegStep(UserID, 1);
                                bot.PushMessage(UserID, "請問您的姓名?");
                               
                                break;
                            case "信箱修改":
                                SetUserEmail(UserID,"");
                                SetUserRegStep(UserID, 0);
                                bot.PushMessage(UserID, "請問公司的專屬信箱?");
                               
                           
                                break;
                            case "姓名確認":
                                Random Rnd = new Random();
                                string validCode = Rnd.Next(0, 10).ToString()+ Rnd.Next(0, 10).ToString()+ Rnd.Next(0, 10).ToString()+ Rnd.Next(0, 10).ToString();
                                System.Diagnostics.Trace.WriteLine("validCode:" + validCode.ToString());
                                SetValidCode(UserID, validCode);
                                //email
                                /*
                                  System.Diagnostics.Trace.WriteLine("apiKey:" + apiKey);
                                  var client = new SendGridClient(apiKey);
                                  var from = new EmailAddress("slowmocross@gmail.com", "奧衍股份有限公司");
                                  var subject = "請假系統註冊驗證信";


                                  var reciver = GetUserEmail(UserID);
                                System.Diagnostics.Trace.WriteLine("reciver:" + reciver);
                                var to = new EmailAddress(reciver, "Example User");
                                var plainTextContent = "";
                                var htmlContent = "<strong>您的驗證碼為:"+ validCode + "</strong>";
                                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                                  var response =await  client.SendEmailAsync(msg);
                                  System.Diagnostics.Trace.WriteLine("response StatusCode:" + response.StatusCode);
                                 System.Diagnostics.Trace.WriteLine("response:" + response.Body.ToString());
                                 */
                                //
                              /*  var success = await EmailSender.SendEmail("Sending with Twilio SendGrid is Fun");
                                if (!success)
                                    System.Diagnostics.Trace.WriteLine("I was not able to send your message. Something went wrong.");
                                else
                                {
                                    System.Diagnostics.Trace.WriteLine("Thanks for the feedback.");
                                   
                                }*/
                                //
                                bot.PushMessage(UserID, "您的驗證碼為?");
                                SetUserRegStep(UserID, 2);

                                break;
                            case "姓名修改":
                                SetUserEmail(UserID, "");
                                SetUserRegStep(UserID, 1);
                                bot.PushMessage(UserID, "請問您的姓名?");
                                break;
                            case "類型事假":
                            case "類型病假":
                            case "類型公假":
                            case "類型特休":
                                var leaveTypeConfirm = new ConfirmTemplate();
                                leaveTypeConfirm.altText = "無法顯示時的替代文字";

                                var leaveType= symbol.Substring(2, 2);
                                System.Diagnostics.Trace.WriteLine("leaveType:"+ leaveType);
                                leaveTypeConfirm.text = "請假類型為:" + leaveType + "? ";
                                var leaveTypeConfirmtActions = new List<TemplateActionBase>();
                                leaveTypeConfirmtActions.Add(new PostbackAction() { label = "確認", data = "類型確認" + DateTime.Now });
                                leaveTypeConfirmtActions.Add(new PostbackAction() { label = "修改", data = "類型修改" + DateTime.Now });
                                leaveTypeConfirm.actions = leaveTypeConfirmtActions;
                                SetLeaveType(UserID, leaveType);
                                bot.PushMessage(UserID, leaveTypeConfirm);
                              

                                break;
                            case "類型確認":
                                SetUserLeaveStep(UserID, 1);

                                var startTimeMsg = new ButtonsTemplate();
                                startTimeMsg.altText = "無法顯示時的替代文字";
                                startTimeMsg.text = "請假開始時間?";
                                startTimeMsg.title = "";
                                var actions = new List<TemplateActionBase>();
                                actions.Add(new DateTimePickerAction() { label="開始時間",mode="datetime",data= "開始時間" + DateTime.Now });
                                startTimeMsg.actions = actions;
                                bot.PushMessage(UserID, startTimeMsg);

                                break;
                            case "類型修改":
                                SetLeaveType(UserID, "");
                                break;
                            case "開始時間":
                                DateTime startTime =  DateTime.Parse(LineEvent.postback.Params.datetime);
                                SetStartTime(UserID, startTime);
                                //  .ToString("yyyy/MM/dd HH:mm:ss")
                                System.Diagnostics.Trace.WriteLine("time:" + startTime.ToString("yyyy/MM/dd HH:mm:ss"));
                                System.Diagnostics.Trace.WriteLine("Year:" + startTime.Year.ToString());
                                System.Diagnostics.Trace.WriteLine("Month:" + startTime.Month.ToString());
                                System.Diagnostics.Trace.WriteLine("Day:" + startTime.Day.ToString());
                                var endTimeMsg = new ButtonsTemplate();
                                endTimeMsg.altText = "無法顯示時的替代文字";
                                endTimeMsg.text = "請假結束時間?";
                                endTimeMsg.title = "";
                                actions = new List<TemplateActionBase>();
                                actions.Add(new DateTimePickerAction() { label = "結束時間", mode = "datetime", data = "結束時間" + DateTime.Now });
                                endTimeMsg.actions = actions;
                                bot.PushMessage(UserID, endTimeMsg);

                                break;

                            case "結束時間":
                                var leaveTimeConfirm = new ConfirmTemplate();
                                leaveTimeConfirm.altText = "無法顯示時的替代文字";
                                //判斷是當天還多天
                                DateTime endTime = DateTime.Parse(LineEvent.postback.Params.datetime);
                               
                                SetEndTime(UserID, endTime);
                                System.Diagnostics.Trace.WriteLine("StartTime:" + GetStartTime(UserID).ToString("yyyy/MM/dd"));
                                System.Diagnostics.Trace.WriteLine("endTime:" + endTime.ToString("yyyy/MM/dd"));
                                if(GetStartTime(UserID).ToString("yyyy/MM/dd")== endTime.ToString("yyyy/MM/dd"))
                                {
                                    System.Diagnostics.Trace.WriteLine("同天");
                                    leaveTimeConfirm.text = "請假時間為:" + GetStartTime(UserID).ToString("yyyy/MM/dd HH:mm") + "到" + endTime.ToString("yyyy/MM/dd HH:mm");
                                    var leaveTimeConfirmActions = new List<TemplateActionBase>();
                                    leaveTimeConfirmActions.Add(new PostbackAction() { label = "確認", data = "請假確認" + DateTime.Now });
                                    leaveTimeConfirmActions.Add(new PostbackAction() { label = "修改", data = "請假修改" + DateTime.Now });
                                    leaveTimeConfirm.actions = leaveTimeConfirmActions;
                                }
                                else
                                {
                                    System.Diagnostics.Trace.WriteLine("不同天");
                                    //多天
                                    leaveTimeConfirm.text = "請假時間為:" + GetStartTime(UserID).ToString("yyyy/MM/dd") + "到" + endTime.ToString("yyyy/MM/dd");
                                    var leaveTimeConfirmActions = new List<TemplateActionBase>();
                                    leaveTimeConfirmActions.Add(new PostbackAction() { label = "確認", data = "請假確認" + DateTime.Now });
                                    leaveTimeConfirmActions.Add(new PostbackAction() { label = "修改", data = "請假修改" + DateTime.Now });
                                    leaveTimeConfirm.actions = leaveTimeConfirmActions;
                                }
                                bot.PushMessage(UserID, leaveTimeConfirm);

                                break;

                            case "請假確認":
                                bot.PushMessage(UserID, "假單已送出");
                                insertPendingLeave(UserID, GetLeaveType(UserID), GetStartTime(UserID), GetEndTime(UserID), 5, "reason", "");
                                break;

                            case "請假修改":
                                SetUserLeaveStep(UserID, 1);
                                startTimeMsg = new ButtonsTemplate();
                                startTimeMsg.altText = "無法顯示時的替代文字";
                                startTimeMsg.text = "請假開始時間?";
                                startTimeMsg.title = "";
                                actions = new List<TemplateActionBase>();
                                actions.Add(new DateTimePickerAction() { label = "開始時間", mode = "datetime", data = "開始時間" + DateTime.Now });
                                startTimeMsg.actions = actions;
                                bot.PushMessage(UserID, startTimeMsg);
                                break;
                        }
                    }
                        
                
                }
               


                return Ok();
            }
            catch(Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message.ToString());
                return Ok();
            }
        }






        private bool isRegister(string UserID)
        {
            if (getRegisterStatus(UserID))//先從記憶體查 在去WEB SQL查
            {
                System.Diagnostics.Trace.WriteLine("你已經註冊過了");
                return true;
            }
            else
            {
                if (checkRegister(UserID) == 0)
                {

                    return false;
                }
                else if (checkRegister(UserID) == 1)
                {//把信箱 名字 存到記憶體裡
                    SetRegisterStatus(UserID, true);
                    System.Diagnostics.Trace.WriteLine("你已經註冊過了");
                    return true;
                }
                else
                {
                    System.Diagnostics.Trace.WriteLine("查詢出錯");
                    return false;
                }
              
            }
        }
    }






}
