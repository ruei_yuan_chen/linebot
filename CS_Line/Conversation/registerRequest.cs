﻿using isRock.LineBot.Conversation;
using System;
using System.Web;

namespace CS_Line.Conversation
{
    public class registerRequest : ConversationEntity
    {
        [Question("請問公司的專屬信箱?")]
        [Order(1)]
        public string Email { get; set; }

        [Question("請問您的姓名?")]
        [Order(2)]
        public string Name { get; set; }


       /* [Question("請問您的驗證碼?")]
        [Order(3)]
        public int ValidCode { get; set; }*/
    }
}
