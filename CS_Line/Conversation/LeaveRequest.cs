﻿using isRock.LineBot.Conversation;
using System;
using System.Web;

namespace CS_Line.Conversation
{
    public class LeaveRequest : ConversationEntity
    {
        [ButtonsTemplateQuestion("詢問", "請問您要請的假別是?", "https://s.zimedia.com.tw/s/Tf2UAh-1", "事假", "病假", "公假", "婚假")]
        [Order(1)]
        public string 假別 { get; set; }

        [Question("請問您的請假日期?")]
        [Order(2)]
        public DateTime 請假日期 { get; set; }

        [Question("請問您的請假日期?")]
        [Order(3)]
        public DateTime 開始時間 { get; set; }
    }
}
